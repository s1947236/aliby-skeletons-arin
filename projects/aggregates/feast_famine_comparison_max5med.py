#!/usr/bin/env jupyter
# ---
# jupyter:
#   jupytext:
#     cell_metadata_filter: title,-all
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% Optional title [markdown]
"""
## Notebook that explores the fluorescence during consecutive feast and famine periods for single-cell data from ALIBY.
"""

# %%
from postprocessor.grouper import NameGrouper

fpath = "/home/alan/Documents/dev/skeletons/scripts/data/19203_2020_09_30_downUpshift_twice_2_0_2_glu_ura8_ura8h360a_ura8h360r_00"
grouper = NameGrouper(fpath)

# %% Experiment overview
"""
First, get the number of cells per group, to aknowledge differences in sample sizes.
"""

# %% * Optional title 2
import seaborn as sns

# %% Experiment overview
# 1. Check the overall number of cells in a given experiment.

# %% [markdown]
"""
Check the noise of all relevant signals
"""

# %% Experiment overview
am = grouper.aggregate_multisignals("/extraction/general/None/area")
kymo_agg = grouper.concat_signal("/extraction/GFP/np_max/max5px")
med = grouper.concat_signal("/extraction/GFP/np_max/median")

m5_med = kymo_agg / med
import numpy as np

# Convert back to ntps
spans = (0, *[x[1] * 60 // grouper.tinterval for x in grouper.stages_span])
cumsum = np.cumsum(spans)
slices = [
    slice(start, min(end, kymo_agg.columns[-1]))
    for start, end in zip(cumsum[:-1], cumsum[1:])
]

# Split df
dfs_split = [m5_med.iloc(axis=1)[slc] for slc in slices]
df = dfs_split[0]

concat = pd.concat(
    [df.mean(axis=1) for df in dfs_split],
    axis=1,
)

# %% * Plotting tools and naming


import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns

sns.set_context("paper")
sns.set_style("darkgrid")

# stage_names = [
#     f"{i}:{stage}" for i, stage in enumerate(grouper.stages[: len(dfs_split)])
# ]

# %% [markdown]
"""
We plot the average max5/median to see the distribution in all our environmental stages
"""
# %%
from string import ascii_uppercase as au
from matplotlib.offsetbox import AnchoredText

base_names = ("Feast", "Famine")
stage_names = [f"{base_names[i % 2]} {( i // 2 )+1}" for i in range(len(dfs_split))]
concat.columns = stage_names

fig, subplots = plt.subplots(2, 2, sharex="col", sharey="row")
# fig, subplots = plt.subplots(2, 2)
for i, (ax, (stage_x, stage_y)) in enumerate(
    zip(subplots.flatten(), ((2, 0), (1, 0), (2, 3), (1, 3)))
):
    sns.kdeplot(
        data=np.log(concat[::-1]),
        x=stage_names[stage_x],
        y=stage_names[stage_y],
        hue="group",
        fill=True,
        alpha=0.4,
        ax=ax,
    )
    if i:
        ax.legend().set_visible(False)
    else:
        sns.move_legend(
            ax,
            "center",
            bbox_to_anchor=(1.1, -0.05),
            ncol=3,
            title="strain",
            frameon=False,
        )

    anc = AnchoredText(
        au[i], loc="best", frameon=False, prop=dict(fontsize=15, ha="center")
    )
    ax.add_artist(anc)

plt.ylim(0, 0.7)
plt.xlim(0, 0.7)
plt.savefig("removeme.png", dpi=500)
plt.close()

# %% [markdown]
"""
Single-cell comparisons of aggregation metrics for consecutive periods of feast and famine.
The aggregation metric we are using is the five pixels with the highest fluorescent value divided by the median value for a specific cell.
*A.* During feast periods all strains sustain low aggregation at similar levels, although the superaggregator mutant, ura8h360r, has a slightly higher aggregation at basal levels.
*B.* During the first famine period all strains respond by aggregating URA proteins into filaments, but they do so at different levels:  The superaggregator and the wild type respond much more strongly than the low aggregation strain.
*C.* In a similar fashion to panel B, when comparing the second period of feast and famine we can see the aggregation occurring again, although not at the same intensity for the wild type strain.
*D.* When comparing aggregation at the first and second famine periods we can see how the superaggregator and low aggregators reacted in a similar way, their slopes are close to the diagonal, yet the wild-type's second famine was les pronounced than the first one, which implies a memory mechanism that reduces the tendency to move into the aggregation state.
Media conditions are synthetic complete (SC) in 2% glucose when for Feast, and the same media but no glucose for Famine.
The number of cells for each strain are as follows:  266 cells for the wild type (ura8), 296 for the low aggregation mutant (urah360a) and 714 for the superaggregator one (ura8h360r).
"""
# %%
grouper.nretained
