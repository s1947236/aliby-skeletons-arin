# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.11.2
#   kernelspec:
#     display_name: baby
#     language: python
#     name: baby
# ---

# %%
#!/usr/bin/env python3

from aliby.pipeline import PipelineParameters, Pipeline

params = PipelineParameters.default(
    general={
        "expt_id": 19993,
        "distributed": 2,
        "directory": "../data/",
        "server_info": {
            "host": "***REMOVED***",
            "username": ***REMOVED***,
            "password": "***REMOVED***",
        },
    }
)
p = Pipeline(params)

p.run()
