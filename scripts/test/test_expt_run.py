#!/usr/bin/env python3
# Script to test the first position and timepoint of many experiments
# the repl output can then be used to find bugs in the code or positions that should be deleted.

import tensorflow as tf

tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

from aliby.pipeline import PipelineParameters, Pipeline

f = open("myexpts.txt", "r")
for line in f.readlines():
    exptid = int(line)
    try:
        params = PipelineParameters.default(
            general={
                "expt_id": exptid,
                "distributed": 0,
                "tps": 1,
                "strain": 0,
                "server_info": {
                    "host": "***REMOVED***",
                    "username": ***REMOVED***,
                    "password": "***REMOVED***",
                },
            }
        )
        p = Pipeline(params)
        p.run()
        print("Experiment {} works.".format(exptid))
    except Exception as e:
        print("Failed experiment {}. Error: {}".format(exptid, e))
