#!/usr/bin/env jupyter

from extraction.core.extractor import Extractor, ExtractorParameters
from aliby.tile.tiler import Tiler
from aliby.io.image import Image

fpath = "/home/alan/Documents/dev/skeletons/scripts/data/19993_2021_06_15_pypipeline_unit_test_00/pos001.h5"


params = ExtractorParameters.from_meta(fpath).to_dict()
params["tree"]["GFP"]["None"] = {"nuc_conv_3d"}

results = None
with Image.from_h5(fpath) as img:
    tiler = Tiler.from_hdf5(img, fpath)
    ext = Extractor.from_tiler(ExtractorParameters.from_dict(params), fpath, tiler)
    ext.load_custom_funs()
    results = ext.run(save=False)

assert results is not None, "Results were not produced"
assert len(
    ["nuc_conv_3d" in key for key in results.keys()]
), "nuc_conv_3d was not produced"
