#!/usr/bin/env python3
# Check that the linear Cells class works fine
from agora.io.cells import CellsLinear

fpath = "/home/alan/Documents/dev/skeletons/scripts/data/19447_2020_11_18_downUpshift_2_0_2_glu_gcd2_gcd6_gcd7__02/gcd2_001.h5"

cells = CellsLinear(fpath)

tp0 = cells.at_time(0)
lbls0 = cells.labels_at_time(0)

matches = [
    len(x) == len(y) for trap, x, y in zip(tp0.keys(), tp0.values(), lbls0.values())
]

import numpy as np

assert np.all(
    matches
), f"Labels and masks don't match in tps {np.where(~np.array(matches))[0]}"
