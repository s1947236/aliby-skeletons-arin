#!/usr/bin/env python3

import h5py
from postprocessor.core.processor import PostProcessorParameters, PostProcessor

from pathlib import Path

fpath = Path(
    "/home/alan/Documents/dev/libs/aliby/data/sofia/2021_08_24_2Raf_00/2021_08_24_2Raf_00/pic2_001.h5"
)

from agora.io.signal import Signal

s = Signal(fpath)
vol = s["extraction/general/None/volume"]
# vol = s["postprocessing/births/extraction_general_None_volume"]
# import seaborn as sns
# import matplotlib.pyplot as plt

# sns.heatmap(vol.sort_index(), robust=True)
# plt.show()
