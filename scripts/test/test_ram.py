#!/usr/bin/env python3


from aliby.pipeline import PipelineParameters, Pipeline

expt_id = 20290
# expt_id = 19993
params = PipelineParameters.default(
    general={
        "expt_id": expt_id,
        "distributed": 0,
        "server_info": {
            "host": "***REMOVED***",
            "username": ***REMOVED***,
            "password": "***REMOVED***",
        },
        "strain": "d1134_016",
    },
    tiler={"tile_size": 96},
)
p = Pipeline(params)
p.run()
