# Check all trap centres
from pathlib import Path
import h5py
import matplotlib.pyplot as plt

dirpath = Path(
    "/home/alan/Documents/dev/stoa_libs/pipeline-core/data/2021_11_24_Bub1_arrest_test_00/2021_11_24_Bub1_arrest_test_00/"
)


def get_tlocs(fpath):
    with h5py.File(fpath, "r") as f:
        tlocs = f["trap_info/trap_locations"][()]
    return tlocs


def plot_tlocs(tlocs):
    return plt.scatter(tlocs[:, 0], tlocs[:, 1])


for f in dirpath.rglob("*.h5"):
    print(f)
    tlocs = get_tlocs(f)
    plot_tlocs(tlocs)
    plt.show()
