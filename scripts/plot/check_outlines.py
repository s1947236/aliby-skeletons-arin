#!/usr/bin/env python3
from pathlib import Path

from aliby.grouper import NameGrouper

path = Path(
    # "/home/alan/Documents/dev/stoa_libs/pipeline-core/data/2021_11_04_doseResponse_raf_1_15_2_glu_01_2_dual_phluorin_whi5_constantMedia_00/2021_11_04_doseResponse_raf_1_15_2_glu_01_2_dual_phluorin_whi5_constantMedia_00"
    # "/home/alan/Documents/dev/stoa_libs/pipeline-core/data/2021_11_24_Bub1_arrest_test_00/2021_11_24_Bub1_arrest_test_00/"
    "/home/alan/Documents/dev/stoa_libs/pipeline-core/data/Bub1_arrest_test_bak/2021_11_24_Bub1_arrest_test_00"
)
ng = NameGrouper(path)
# agg = ng.aggregate_multisignals(["extraction/general/None/area"], pool=0)
# agg = ng.concat_signal("extraction/general/None/volume")
agg_gr = ng.concat_signal("postprocessing/dsignal/extraction_general_None_volume")

df = agg_gr[agg_gr.notna().sum(axis=1) > agg_gr.shape[1] * 0.8]
df = df.apply(lambda x: x.rolling(15).mean(), axis=1)
sns.heatmap(df, robust=True)
plt.show()

s = ng.signals["IL102_017"]

from aliby.cells import Cells

c = Cells.from_source(s.filename)
masks = c.at_time(0)

import numpy as np

np.sum(masks[0], axis=0).shape
