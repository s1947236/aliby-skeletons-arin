#!/usr/bin/env jupyter

"""
Check that cell labels within traps are continuous
"""
from aliby.grouper import NameGrouper
from agora.io.cells import Cells

fpath = "/home/alan/Documents/dev/skeletons/scripts/data/19203_2020_09_30_downUpshift_twice_2_0_2_glu_ura8_ura8h360a_ura8h360r_00"
grouper = NameGrouper(fpath)
incoherences = {}
for name, s in grouper.signals.items():
    with h5py.File(s.filename, "r") as f:
        cell = f["extraction/general/None/area/cell_label"][()]
        trap = f["extraction/general/None/area/trap"][()]

    for trap_id in range(Cells(s.filename).ntraps):
        cells_in_trap = cell[trap == trap_id]
        # If max(cell_label) != len(cell_label) it means there are missing ids
        if cells_in_trap.any():
            print(len(cell[trap == trap_id]) == max(cells_in_trap))
        else:
            if s.filename.stem in incoherences:
                incoherences[s.filename.stem].append(trap_id)
            else:
                incoherences[s.filename.stem] = [trap_id]
