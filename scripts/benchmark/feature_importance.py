#!/usr/bin/env jupyter
"""
Plot feature importances of a main and backup models to decide onto which features it is worth keeping
"""
from baby.tracker.core import CellTracker

import pandas as pd
import seaborn as sns

ct = CellTracker()
fig, axes = plt.subplots(2, 1, sharex=True)
for i, model_name in enumerate(("model", "bak_model")):
    sns.barplot(
        data=pd.DataFrame(
            {
                "Feature importance": getattr(ct, model_name).feature_importances_,
                "Feature": getattr(ct, model_name).all_ofeats,
            }
        ).sort_values("Feature importance"),
        y="Feature",
        x="Feature importance",
        ax=axes[i],
    )

import matplotlib.pyplot as plt

plt.savefig("FeatureImportance.png", dpi=400)
