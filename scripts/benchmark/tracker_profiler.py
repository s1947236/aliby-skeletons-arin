#!/usr/bin/env jupyter
"""
Development script to to improve baby's tracker to scale better with more crowded images.

This script proved that running the classifier prediction is not the bottleneck
"""

import pickle
from pathlib import Path

from aliby.io.image import ImageDirectory
from aliby.tile.tiler import Tiler, TilerParameters
from baby.tracker.core import CellTracker
from tqdm import tqdm

dpath = Path("/home/alan/Documents/dev/lucias_data/exp_00/pos001/")

channel_paths = {
    "DIC": str(dpath / "CutDIC/*.png"),
    # "Citrine": str(dpath / "CutCitrine/*.png"), # Outcommented because they don't hve the same tps
    # "Dye": str(dpath / "CutDye/*.png"),
    "Segmentation": str(dpath / "Segmentation/Components/*.tif"),
}

# Load Tiler to easily fetch images
tiler_params = TilerParameters.default().to_dict()
for k, v in {"ref_channel": "DIC", "tile_size": None}.items():
    tiler_params[k] = v

tiler = Tiler.from_image(
    ImageDirectory(channel_paths), TilerParameters.from_dict(tiler_params)
)


# 13um pixel size in x100 (I presume). I tested different sizes and this did best
ct = CellTracker(pixel_size=0.125)
from time import perf_counter
import numpy as np

import cProfile


import cProfile

profile = cProfile.Profile()

ntps = 200

profile.enable()
state = None
for tp in tqdm(range(ntps)):
    snapshot = tiler.get_tc(tp, 1)[0]
    t = perf_counter()
    _, state = ct.run_tp(snapshot, state=state, min_distance=0.0001)
    t2 = perf_counter() - t
profile.disable()
profile.dump_stats("tracker_profiler_min_distance.prof")
