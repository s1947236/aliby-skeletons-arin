#!/usr/bin/env python3
from aliby.pipeline import PipelineParameters, Pipeline

params = PipelineParameters.default(
    general={
        "expt_id": 16543,
        "distributed": 4,
        "overwrite": True,
        # "filter": 0,
        "server_info": {
            "host": "***REMOVED***",
            "username": ***REMOVED***,
            "password": "***REMOVED***",
        },
    }
)

p = Pipeline(params)

import cProfile

profile = cProfile.Profile()

profile.enable()
p.run()
profile.disable()
profile.dump_stats("single_pos_speed.prof")
