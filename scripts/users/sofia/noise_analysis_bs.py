#!/usr/bin/env python3


from pathlib import Path
from postprocessor.grouper import NameGrouper
import seaborn as sns
import matplotlib.pyplot as plt

folder = Path(".")
g = NameGrouper(folder)

# GFP median
gfp = g.concat_signal("extraction/GFP/np_max/median", mode="retained")
clean_gfp = gfp.loc[gfp.notna().sum(axis=1) > 120]


candle = gfp.melt(ignore_index=False).reset_index()
sns.lineplot(data=candle, x="variable", y="value", hue="group")
plt.show()

means = clean_gfp.mean(axis=1)

sns.histplot(data=means.reset_index(), x=0, hue="group", stat="probability")
plt.xlabel("Average expression")
plt.ylabel("Count")
