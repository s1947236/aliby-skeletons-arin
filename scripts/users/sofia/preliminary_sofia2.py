d = "/home/alan/Documents/dev/skeletons/data/2021_09_15_1_Raf_06/2021_09_15_1_Raf_06/"

from postprocessor.grouper import NameGrouper
import matplotlib.pyplot as plt
import seaborn as sns

sns.set_theme(style="darkgrid")

grouper = NameGrouper(d)

df = grouper.concat_signal("postprocessing/experiment_wide/aggregated")
gfp = grouper.concat_signal("extraction/GFP/np_max/median")
births = grouper.concat_signal(
    "postprocessing/births/extraction_general_None_volume", mode="mothers"
)
vol = grouper.concat_signal("extraction/general/None/volume")
births = grouper.concat_signal("postprocessing/births/extraction_general_None_volume")


sns.boxplot(data=births.mean(axis=1).reset_index(), x="group", y=0, hue="group")

intersection = set(df.index).intersection(gfp.index).intersection(births.index)

combined = df.loc[intersection]
combined["gfp"] = gfp.median(axis=1).loc[intersection]
combined["births"] = births.sum(axis=1).loc[intersection]

# [ for i in (gfp, vol, births) ]
tmp = births.loc[intersection].loc["d1134", :100]
a = gfp.loc[intersection].loc["d1134", :100].loc[tmp.sum(axis=1) < 7].median(axis=1)
b = vol.loc[intersection].loc["d1134", :100].loc[tmp.sum(axis=1) < 7].max(axis=1)
c = tmp.sum(axis=1)

merged = pd.concat((a, b, c), axis=1)
merged.columns = ["gfp", "vol", "births"]

# with h5py.File(fpath, mode="r") as f:
#     print(f["cell_info/edgemasks/values"][:,0,...])

# Births
sns.boxplot(data=combined.reset_index(), x="group", y="births", hue="group")
# dVol
sns.boxplot(
    data=combined.reset_index(),
    x="group",
    y="dsignal_extraction_general_None_volume_mean",
    hue="group",
)
# GFP
sns.boxplot(data=combined.reset_index(), x="group", y="gfp", hue="group")

# gfp vs dVol
sns.scatterplot(
    data=combined.reset_index(),
    x="gfp",
    y="dsignal_extraction_general_None_volume_max",
    hue="group",
)
# gfp vs Vol
sns.scatterplot(
    data=combined.reset_index(), x="gfp", y="general_volume_mean", hue="group"
)

sns.violinplot(
    data=combined.loc[combined["births"] < 8].reset_index(),
    x="births",
    y="gfp",
    hue="group",
)

sns.regplot(data=combined[combined["births"] < 8], x="gfp", y="births")
sns.scatterplot(
    data=combined[combined["births"] < 8], x="gfp", y="births", hue="group", alpha=0.5
)

plt.show()
