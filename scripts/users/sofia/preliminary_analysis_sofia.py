d = "/home/alan/Documents/dev/skeletons/data/2021_09_15_1_Raf_06/2021_09_15_1_Raf_06/"

from postprocessor.grouper import NameGrouper
import matplotlib.pyplot as plt
import seaborn as sns

sns.set_theme(style="darkgrid")

grouper = NameGrouper(d)

df = grouper.concat_signal("postprocessing/experiment_wide/aggregated")
gfp = grouper.concat_signal("extraction/GFP/np_max/median")
births = grouper.concat_signal(
    "postprocessing/births/extraction_general_None_volume", mode="mothers"
)
vol = grouper.concat_signal("extraction/general/None/volume")
births = grouper.concat_signal("postprocessing/births/extraction_general_None_volume")


sns.boxplot(data=births.mean(axis=1).reset_index(), x="group", y=0, hue="group")

intersection = set(df.index).intersection(gfp.index).intersection(births.index)

combined = df.loc[intersection]
combined["gfp"] = gfp.median(axis=1).loc[intersection]
combined["births"] = births.sum(axis=1).loc[intersection]

# [ for i in (gfp, vol, births) ]
tmp = births.loc[intersection].loc["d1134", :100]
a = gfp.loc[intersection].loc["d1134", :100].loc[tmp.sum(axis=1) < 7].median(axis=1)
b = vol.loc[intersection].loc["d1134", :100].loc[tmp.sum(axis=1) < 7].max(axis=1)
c = tmp.sum(axis=1)

merged = pd.concat((a, b, c), axis=1)
merged.columns = ["gfp", "vol", "births"]

# with h5py.File(fpath, mode="r") as f:
#     print(f["cell_info/edgemasks/values"][:,0,...])
