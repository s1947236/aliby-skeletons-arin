#!/usr/bin/env python3
from pathlib import Path

dpath = Path(
    "/home/alan/Documents/dev/stoa_libs/pipeline-core/data/2021_08_24_2Raf_00/2021_08_24_2Raf_00"
)

pos1 = list(dpath.glob("*016.h5"))[0]

from aliby.io.signal import Signal

s = Signal(pos1)

raw = s.get_raw("extraction/general/None/volume")

import seaborn as sns
import matplotlib.pyplot as plt

sns.heatmap(raw, robust=True)
plt.show()

processed = s["extraction/general/None/volume"].sort_index()
sns.heatmap(processed, robust=True)
plt.show()

clean = processed.loc[processed.notna().sum(axis=1) > 0.8 * 180]

gr = s["postprocessing/dsignal/extraction_general_None_volume"]
clean_gr = gr.loc[gr.notna().sum(axis=1) > 0.8 * 180]
sns.heatmap(clean_gr, robust=True)
plt.show()

from postprocessor.core.processes.savgol import savgolParameters, savgol

sg = savgol(savgolParameters.from_dict({"window": 5, "polynom": 3}))

soft = sg.run(clean_gr)

toplot = processed.loc[63].melt(ignore_index=False, var_name="tp").reset_index()
sns.scatterplot(
    data=toplot,
    x="tp",
    y="value",
    hue="cell_label",
    style="cell_label",
    palette="Pastel1",
)

plt.show()

agg = s["postprocessing/experiment_wide/aggregated"]

sns.histplot(agg["dsignal_extraction_general_None_volume_mean"])
plt.show()
