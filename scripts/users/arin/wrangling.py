#!/usr/bin/env python3

import numpy as np
from postprocessor.core.multisignal.align import align
from postprocessor.core.multisignal.crosscorr import crosscorr
from postprocessor.core.processes.butter import butter
from postprocessor.core.processes.findpeaks import findpeaks
from slicer import slice_interval, slice_strain
from utils import rearrange_by_sorted_list

# TODO: add functionality of choosing only oscillatory flavin time series
def get_dfs_from_slice(
    signal_flavin,
    signal_births,
    strain_name,
    interval_start,
    interval_end,
):
    """
    Choose strain & time interval from aggregrate flavin & birth signal
    DataFrames
    """
    if isinstance(strain_name, str):
        # print("Strain chosen: " + strain_name)
        pass
    elif isinstance(strain_name, list):
        # print("Strain chosen: " + " & ".join(strain_name))
        pass
    else:
        raise TypeError

    # Slice flavin
    strain_flavin_df = slice_strain(signal_flavin, strain_name)
    strain_flavin_df = slice_interval(strain_flavin_df, interval_start, interval_end)
    # Slice births
    strain_births_df = slice_strain(signal_births, strain_name)
    strain_births_df = slice_interval(strain_births_df, interval_start, interval_end)
    # Consistent cell indexes
    common_index = strain_flavin_df.index.intersection(strain_births_df.index)
    strain_births_df = strain_births_df.loc[common_index]
    strain_flavin_df = strain_flavin_df.loc[common_index]

    return strain_flavin_df, strain_births_df

    # Store labels
    # targets = import_labels(
    #    filepath_targets,
    #    signal_flavin_processed.index.get_level_values("cellID"),
    # )

    # Drop non-oscillatory

    # if drop_non_osc:
    #     signal_flavin_processed_osc = signal_flavin_processed.iloc[targets == 1]

    #     print(
    #         "Number of oscillatory cells: {a} ({b:.1f}%)".format(
    #             a=len(signal_flavin_processed_osc),
    #             b=100 * len(signal_flavin_processed_osc) / len(signal_flavin_processed),
    #         )
    #     )
    #     signal_births_processed_osc = signal_births_processed.loc[
    #         signal_flavin_processed_osc.index
    #     ]
    #     signal_flavin_processed = signal_flavin_processed_osc
    #     signal_births_processed = signal_births_processed_osc


class WrangledDataFrameCollection:
    """
    Processes DataFrames using time series analysis and contains the
    intermediate DataFrames produced.
    """

    # def __init__(self, tintervals, critical_freq, births_df, flavin_df, mCherry_df):
    def __init__(self, tintervals, critical_freq, births_df, flavin_df):
        # Filter with Butterworth filter to remove long-term trends
        self.flavin_processed = butter.as_function(
            flavin_df, critical_freq=critical_freq
        )
        # self.mCherry_processed = butter.as_function(
        #     mCherry_df, critical_freq=critical_freq
        # )
        self.births_processed = births_df  # MASK DF

        # Find peaks
        self.flavin_peaks = findpeaks.as_function(self.flavin_processed)  # MASK DF

        # Compute intervals
        # (note: these are lists)
        self.birth_intervals = tintervals * self.mask_to_intervals(
            self.births_processed
        )
        self.flavin_intervals = tintervals * self.mask_to_intervals(self.flavin_peaks)

        # Align time series (then sort by second event interval)
        # ... by peak
        self.flavin_aligned_by_peak, _ = align.as_function(
            self.flavin_processed,
            self.flavin_peaks,
            slice_before_first_event=True,
            events_at_least=1,
        )
        # self.second_ymcs = get_second_intervals(self.flavin_peaks)
        # self.flavin_aligned_by_peak = rearrange_by_sorted_list(
        #     self.flavin_aligned_by_peak, self.second_ymcs
        # )
        self.flavin_aligned_by_peak, _ = self.sort_by_second_interval(
            self.flavin_aligned_by_peak, self.flavin_peaks
        )
        # ... by birth
        self.flavin_aligned_by_birth, self.births_aligned_by_birth = align.as_function(
            self.flavin_processed,
            self.births_processed,
            slice_before_first_event=False,
            events_at_least=2,
        )
        (
            self.flavin_aligned_by_birth,
            self.births_aligned_by_birth,
        ) = self.sort_by_second_interval(
            self.flavin_aligned_by_birth, self.births_aligned_by_birth
        )
        # ... and truncate
        max_shift = self.get_max_shift(self.births_processed, events_at_least=2)
        self.flavin_aligned_by_birth_truncated = self.flavin_aligned_by_birth.iloc[
            :, max_shift:
        ]
        self.flavin_aligned_by_birth_truncated.columns = np.arange(
            len(self.flavin_aligned_by_birth_truncated.columns)
        )
        self.births_aligned_by_birth_truncated = self.births_aligned_by_birth.iloc[
            :, max_shift:
        ]
        self.births_aligned_by_birth_truncated.columns = np.arange(
            len(self.births_aligned_by_birth_truncated.columns)
        )

        # Autocorrelation function
        # time series must all be the same length for cross-correlation,
        # hence cutting stuff from the right where NaNs start to appear
        self.flavin_acfs = crosscorr.as_function(
            self.flavin_processed.dropna(axis=1), connected=True
        )
        # start at zero
        self.flavin_acfs.columns = list(range(len(self.flavin_acfs.columns)))

    def mask_to_intervals(self, mask_df):
        """Convert binary DataFrame, indicating events, to list of intervals"""
        temp_df = mask_df.apply(lambda x: np.diff(np.where(np.array(x) > 0)), axis=1)
        intervals = np.hstack(temp_df.to_list()).ravel()
        return np.array(intervals)

    def mask_to_mean_intervals(self, mask_df):
        """Convert binary DataFrame, indicating events, to list of mean intervals for each cell"""
        temp_df = mask_df.apply(lambda x: np.diff(np.where(np.array(x) > 0)), axis=1)
        mean_intervals = []
        for el in temp_df.to_list():
            if not el.size:
                pass
            else:
                mean_intervals.append(np.mean(el))
        return np.array(mean_intervals)

    def get_max_shift(self, mask_df, events_at_least):
        # Convenience function
        """Get max shift if align process is applied"""
        event_mask = mask_df.apply(lambda x: np.sum(x) >= events_at_least, axis=1)
        mask_df = mask_df.iloc[event_mask.to_list()]
        shift_list = []
        for index in mask_df.index:
            event_locs = np.where(mask_df.loc[index].to_numpy() == 1)[0]
            if event_locs.any():
                shift = event_locs[0]
            else:
                shift = 0
            shift_list.append(shift)
        shift_list = np.array(shift_list)
        return np.max(shift_list)

    def get_second_intervals(self, mask_df):
        """Find intervals between first and second events from a mask DataFrame

        Find intervals between first and second events from a mask DataFrame
        Assumes: already aligned

        Parameters
        ----------
        mask_df : pandas.DataFrame
            binary DataFrame, functions as mask

        Return
        ------
        second_event_intervals : list
            list of intervals

        """
        second_event_intervals = []
        for index in mask_df.index:
            event_locs = np.where(mask_df.loc[index].to_numpy() == 1)[0]
            event_locs = np.delete(
                event_locs, 0
            )  # the first element of this list is always zero
            if event_locs.any():
                second_event_intervals.append(
                    event_locs[0]
                )  # this is when the 2nd event is in relation to the 1st
            else:
                second_event_intervals.append(None)
        # Absence of second event represented by nan
        second_event_intervals = np.array(second_event_intervals, dtype=float)

        return second_event_intervals

    def sort_by_second_interval(self, signal_df_aligned, mask_df_aligned):
        """Sort signal DataFrame and mask DataFrame by length of second interval of mask DataFrame"""
        second_events_intervals = self.get_second_intervals(mask_df_aligned)
        signal_df_aligned_sorted = rearrange_by_sorted_list(
            signal_df_aligned, second_events_intervals
        )
        mask_df_aligned_sorted = rearrange_by_sorted_list(
            mask_df_aligned, second_events_intervals
        )
        return signal_df_aligned_sorted, mask_df_aligned_sorted
