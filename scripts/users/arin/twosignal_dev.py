#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from pathlib import Path
from postprocessor.core.processes.butter import butter
from postprocessor.core.multisignal.crosscorr import crosscorr
from postprocessor.routines.median_plot import median_plot
from postprocessor.grouper import NameGrouper
from slicer import slice_interval, slice_strain

# from statsmodels.tsa.stattools import grangercausalitytests

### IMPORT AND SELECT DATA

# HDF5 -- Import files, group by strain, choose signals
if True:
    # Group by strain
    data_directory = Path(
        "/home/arin/git/data/26643_2022_05_23_flavin_htb2_glucose_20gpL_01_00/"
    )
    grouper = NameGrouper(data_directory)
    # experimentID = grouper.name[0:5]

    # Choose signals
    signal_flavin = grouper.concat_signal("extraction/Flavin_bgsub/np_max/mean")
    signal_mCherry = grouper.concat_signal("extraction/mCherry_bgsub/np_max/mean")
    signal_births = grouper.concat_signal(
        "postprocessing/births/extraction_general_None_volume"
    )

# Create synthetic signals to test processes
if True:
    dummy1 = np.tile(np.sin(np.linspace(-np.pi, 10 * np.pi, 200)), (20, 1))
    dummy2 = np.tile(
        np.sin(np.linspace(-np.pi, 10 * np.pi, 200) - 0.3), (20, 1)
    )  # shift to the right
    dummy1 += 0.05 * np.random.rand(*dummy1.shape)
    dummy2 += 0.05 * np.random.rand(*dummy2.shape)
    signal_dummy1 = pd.DataFrame(dummy1)
    signal_dummy2 = pd.DataFrame(dummy2)

# Choose strain & interval
if True:
    strain_name = "htb2mCherry"
    interval_start = 5
    interval_end = 288
    critical_freq = 1 / 350

    strain_flavin_df = slice_interval(
        slice_strain(signal_flavin, strain_name), interval_start, interval_end
    )
    strain_mCherry_df = slice_interval(
        slice_strain(signal_mCherry, strain_name), interval_start, interval_end
    )
    strain_flavin_processed = butter.as_function(
        strain_flavin_df, critical_freq=critical_freq
    )
    strain_mCherry_processed = butter.as_function(
        strain_mCherry_df, critical_freq=critical_freq
    )

# Cross-correlation on real data
if True:
    crosscorr_result = crosscorr.as_function(
        strain_flavin_processed, strain_mCherry_processed
    )

# Cross-correlation on fake data
if True:
    crosscorr_result = crosscorr.as_function(signal_dummy1)

# Cross-correlation on shifted real data
if True:
    shift = 10
    strain_flavin_processed_shifted = pd.DataFrame(
        np.roll(strain_flavin_processed.values, shift, axis=1),
        index=strain_flavin_processed.index,
        columns=strain_flavin_processed.columns,
    )
    crosscorr_result = crosscorr.as_function(
        strain_flavin_processed, strain_flavin_processed_shifted
    )

# Cross-correlation plots

# heatmap
if True:
    sns.heatmap(crosscorr_result, cmap="RdBu", robust=True)

# median plot
if True:
    median_plot(
        trace_df=crosscorr_result,
        trace_name="cross-correlation",
        unit_scaling=5,
        label="FY4 HTB2::mCherry",
        ylabel="Cross-correlation function",
        plot_title="flavin v. mCherry cross-correlation",
    )
    plt.axhline(0, color="r")
    plt.axvline(0, color="r")

# Granger causality
if False:
    grangercausality_dict = grangercausalitytests(
        pd.concat(
            [
                strain_mCherry_processed.T.iloc[:, 0],
                strain_flavin_processed.T.iloc[:, 0],
            ],
            axis=1,
        ),
        maxlag=[3],
    )

# Granger causality loop-through population
if False:
    f_values = []
    p_values = []
    for cellno in range(len(strain_mCherry_processed)):
        grangercausality_dict = grangercausalitytests(
            pd.concat(
                [
                    strain_mCherry_processed.T.iloc[:, cellno],
                    strain_flavin_processed.T.iloc[:, cellno],
                ],
                axis=1,
            ),
            maxlag=[3],
        )
        f_values.append(grangercausality_dict[3][0]["ssr_ftest"][0])
        p_values.append(grangercausality_dict[3][0]["ssr_ftest"][1])
    plt.scatter(f_values, -np.log2(p_values))
    plt.xlabel("F")
    plt.ylabel("-log2(p)")
    plt.show()
