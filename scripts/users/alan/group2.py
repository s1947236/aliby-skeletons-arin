#!/usr/bin/env python3


from pcore.grouper import NameGrouper

folder = Path(
    "/home/alan/Documents/dev/stoa_libs/pipeline-core/data/2021_11_04_doseResponse_raf_1_15_2_glu_01_2_dual_phluorin_whi5_constantMedia_00/"
)
g = NameGrouper(folder)
# g.signals = {k: v for k, v in g.signals.items() if str(v) != filename}
# signame = "postprocessing/experiment_wide/aggregated"
# signame = "extraction/general/None/volume"


# df = g.signals["raf_2_023"]["extraction/general/None/volume"]
# df = list(g.signals.values())[0]["postprocessing/births/extraction_general_none_volume"]
# s = list(g.signals.values())[0]
# import h5py

# with h5py.File(s.filename) as f:
#     print(f["postprocessing/births/extraction_general_None_volume"].keys())
# for k, s in g.signals.items():
#     print(k)
#     print(s["postprocessing/births/extraction_general_None_volume"])
# df2 = df[df.notna().sum(axis=1) > 80]
# sns.heatmap(df2, robust=True)
# plt.show()


# c = g.concat_signal("extraction/general/None/area", pool=8)
c = g.concat_signal("postprocessing/births/extraction_general_None_volume", pool=8)

import seaborn as sns
import matplotlib.pyplot as plt

c.columns = list(range(0, 900, 5))
sns.set_context("talk")
sns.heatmap(c.dropna(how="any"), cbar=False)
plt.ylabel("Cells")
plt.xlabel("Time (minutes)")
plt.show()

sns.set_theme(style="whitegrid")
sns.set_context("talk")
vol = g.signals["Bub2D_011"]["extraction/general/None/volume"]
vol = g.signals["IL143_024"]["extraction/general/None/volume"]
vol = g.signals["IL143_024"].get_raw("extraction/general/None/volume")
sns.heatmap(vol.sort_index(), robust=True)
plt.show()


switch = 84
d1 = c.iloc[:, 20:switch]
d2 = c.iloc[:, switch:]
divh1 = pd.DataFrame(d1.apply(np.mean, axis=1), columns=["div/h"]) * 12
divh1["condition"] = "happy"
divh2 = pd.DataFrame(d2.apply(np.mean, axis=1), columns=["div/h"]) * 12
divh2["condition"] = "sad"
combined = pd.concat((divh1.reset_index(), divh2.reset_index()))
import pandas as pd

sns.violinplot(
    data=combined,
    x="group",
    y="div/h",
    split=True,
    palette="muted",
    hue="condition",
)
plt.show()


melted = (
    c.iloc[:, 1:]
    .groupby("group")
    .apply(np.mean)
    .melt(ignore_index=False, var_name="tp", value_name="avg div")
    .reset_index()
)
melted["time (mins)"] = melted["tp"] * 5

sns.lineplot(data=melted, x="time (mins)", y="avg div", hue="group")
plt.show()


def get_df(signame):
    c = g.concat_signal(signame)
    d = c.loc[c.notna().sum(axis=1) > 60]
    # for i in c.columns:
    #     if "ratio" in i:
    #         c[i] = 1 / c[i]

    # splits = [x.split("_") for x in c.columns]
    # new = [
    #     [
    #         x
    #         for x in s
    #         if x
    #         not in [
    #             "general",
    #             "metric",
    #             "postprocessing",
    #             "extraction",
    #             "None",
    #             "np",
    #             "max",
    #             "em",
    #         ]
    #     ]
    #     for s in splits
    # ]
    # joint = ["_".join(n) for n in new]
    # c.columns = joint

    return d


signame = (
    "postprocessing/dsignal/postprocessing_bud_metric_extraction_general_None_volume"
)
signame = "extraction/mCherry/np_max/max2p5pc"
window = 15
whi5 = get_df(signame)
whi5 = whi5.div(whi5.mean(axis=1), axis=0)
whi5_movavg = whi5.apply(lambda x: pd.Series(moving_average(x.values, window)), axis=1)
ph = 1 / get_df("extraction/em_ratio/np_max/median")
ph = ph.div(ph.mean(axis=1), axis=0)
ph_movavg = ph.apply(lambda x: pd.Series(moving_average(x.values, window)), axis=1)
ph_norm = ph.iloc(axis=1)[window // 2 : -window // 2] / ph_movavg
whi5_norm = whi5.iloc(axis=1)[window // 2 : -window // 2] / whi5_movavg
rand = np.random.randint(len(whi5), size=4)

melted = ph_norm.iloc[rand].melt(ignore_index=False).reset_index()
melted["signal"] = "ph"
combined = whi5_norm.iloc[rand].melt(ignore_index=False).reset_index()
combined["signal"] = "whi5"
combined = pd.concat((melted, combined))
combined["t_id"] = [str(x) + y for x, y in zip(combined["trap"], combined["position"])]
h = sns.FacetGrid(combined, col="t_id", col_wrap=2)
h.map_dataframe(sns.scatterplot, x="variable", y="value", hue="signal")
h.add_legend()
plt.show()


def moving_average(a, n=3):
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1 :] / n


import numpy as np

import seaborn as sns
from matplotlib import pyplot as plt

sns.set_style("darkgrid")
sns.lineplot(
    data=d.melt(ignore_index=False).reset_index("group"),
    x="variable",
    y="value",
    # y="gsum_median_mean",
    # y="dsignal_postprocessing_bud_metric_extraction_general_None_volume_max",
    # y="dsignal_extraction_general_None_volume_max",
    # hue="position",
    hue="group",
    # size="general_volume",
    # alpha=0.5,
    palette="muted",
    # ci=None,
)
# plt.xlim((0, 5))
plt.show()
