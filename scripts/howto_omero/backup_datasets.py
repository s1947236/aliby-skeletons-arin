#!/usr/bin/env jupyter

"""
Backup a given experiment and their metadata.
Script developed after a temporal dual failure in a NAS drive. Use when wanting to save compressed local copies of data.
"""
import os
import shutil
import typing as t
from pathlib import Path, PosixPath

from aliby.io.dataset import Dataset
from aliby.io.image import Image
from numcodecs import Blosc
from p_tqdm import p_imap

# from pathos.multiprocessing import Pool

# dataset_id = 19993
alan = [
    19993,
    19996,
    18617,
    19129,
    19144,
    19169,
    19232,
    19307,
    19310,
    19311,
    19328,
    20419,
    19810,
    19447,
    19203,
    20419,
    19232,
    16543,
    19311,
    16545,
    20419,
]

arin = [
    19972,
    19979,
    20016,
    20212,
    23174,
    25681,
    26643,
    27753,
    27895,
    27917,
    27928,
    27993,
    28018,
    28041,
    28101,
    29737,
]
# dataset_id = 19972

ome_server_info = dict(
    host="***REMOVED***", username=***REMOVED***, password="***REMOVED***"
)
# target_location = Path("./backup")
target_location = Path("/mnt/datastore/Swainlab/omero_backup")
num_cpus = 11
try:
    os.mkdir(target_location)
except:
    pass


# with Image(target_id, **credentials) as ome_wrapper:
def get_images_to_backup(
    dataset_id: int,
    ome_server_info: t.Dict[str, str],
    target_location: t.Union[str, PosixPath],
):
    with Dataset(dataset_id, **ome_server_info) as ali_dataset:
        dataset_location = Path(target_location) / ali_dataset.unique_name
        shutil.rmtree(dataset_location / "*.zarr", ignore_errors=True)
        os.makedirs(dataset_location, exist_ok=True)

        ali_dataset.cache_logs(dataset_location)
        return {
            dataset_location / f"{ image_local_name }.zarr": ome_image_id
            for image_local_name, ome_image_id in ali_dataset.get_images().items()
        }


def backup_image(
    ome_image_id: int,
    ome_server_info: t.Dict[str, str],
    target_location: PosixPath,
    **kwargs,
):
    # compressor = Blosc(cname='lz4', clevel=5, shuffle=Blosc.BITSHUFFLE)

    # Delete existing data and overwrite
    with Image(ome_image_id, **ome_server_info) as ali_image:
        ali_image.data.to_zarr(target_location, compute=False, **kwargs).compute(
            scheduler="synchronous"
        )


# backup_dataset(dataset_id, ome_server_info, target_location)
dataset_ids = [*alan, *arin]
images_to_backup = {
    k: v
    for dataset_id in dataset_ids
    for k, v in get_images_to_backup(
        dataset_id, ome_server_info, target_location
    ).items()
}

compressor = Blosc(cname="zlib", clevel=9)
# with Pool(num_cpus) as p:
results = p_imap(
    # results = p.map(
    lambda x: backup_image(
        ome_image_id=x[1],
        ome_server_info=ome_server_info,
        target_location=x[0],
        compressor=compressor,
    ),
    images_to_backup.items(),
    # num_cpus=num_cpus,
)
