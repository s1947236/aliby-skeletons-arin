#!/usr/bin/env jupyter
"""
Script that annotates the pixel size of a dataset
"""

import os
import shutil
from pathlib import Path


from omero.model.enums import UnitsLength
from omero.model import LengthI
from agora.io.metadata import MetaData, parse_logfiles
from aliby.io.dataset import Dataset
from aliby.io.image import Image

dataset_id = 19993
credentials = dict(host="***REMOVED***", username=***REMOVED***, password="***REMOVED***")
cache_dir = Path("./local_cache/")

try:
    os.mkdir(cache_dir)
except Exception as e:
    print(e)

# Get z-size and image ids from Metadata files
with Dataset(dataset_id, **credentials) as dataset:
    if dataset.cache_logs(cache_dir):
        parsed_flattened = parse_logfiles(cache_dir)
        z_size = parsed_flattened.get("zsectioning/spacing", [None])[0]
        img_ids = dataset.get_images().values()

# If z_size and dataset is not empty, set all pixel sizes.
if len(img_ids) and z_size:
    for img_id in list(img_ids)[:1]:
        with Image(img_id, **credentials) as img:
            img_size = img.image_wrap.getSizeX()
            if img_size == 1200:  # Prime camera
                pixel_size = 0.182
            elif img_size == 512:  # Evolve camera
                pixel_size = 0.263
            else:
                print(
                    f"Dataset {dataset_id} and image {img_id} don't have an expected pixel size"
                )
                continue
            pixels = img.image_wrap.getPrimaryPixels()._obj

            xy_pix_size = LengthI(pixel_size, UnitsLength.MICROMETER)
            z_pix_size = LengthI(z_size, UnitsLength.MICROMETER)
            pixels.setPhysicalSizeX(xy_pix_size)
            pixels.setPhysicalSizeY(xy_pix_size)
            pixels.setPhysicalSizeZ(z_pix_size)
            img.conn.getUpdateService().saveObject(pixels)

# Clean cache folder
try:
    shutil.rmtree(cache_dir)
except Exception as e:
    print(e)
