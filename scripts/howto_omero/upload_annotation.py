#!/usr/bin/env jupyter
"""
Script showing how to upload a logfile to an existing omero dataset.

"""

from aliby.io.dataset import Dataset
from aliby.io.image import Image

# File used
file_to_upload = "/home/alan/Downloads/pypipeline_unit_testlog.txt"
target_id = 19993
credentials = dict(host="***REMOVED***", username=***REMOVED***, password="***REMOVED***")

# with Image(target_id, **credentials) as ome_wrapper:
with Dataset(target_id, **credentials) as ome_wrapper:
    # dataset.conn.isConnected()
    print(f"No. of annotation files before: {len(ome_wrapper.file_annotations)}")
    ome_wrapper.add_file_as_annotation(file_to_upload)
    print(f"No. of annotation files before: {len(ome_wrapper.file_annotations)}")
