#!/usr/bin/env python3

from aliby.pipeline import PipelineParameters, Pipeline

# IDS to run
# ids = [19199, 19203, 19447] # Ura mutants ones
# ids = [19855]
ids = [19853, 19856]

for i in ids:
    print(i)
    try:
        params = PipelineParameters.default(
            general={
                "expt_id": i,
                "distributed": 6,
                "server_info": {
                    "host": "***REMOVED***",
                    "username": ***REMOVED***,
                    "password": "***REMOVED***",
                },
                # Ensure data will be overwriten
                "override_meta": True,
                "overwrite": True,
            }
        )

        # Change specific leaf in the extraction tree
        params = params.to_dict()
        leaf_to_change = params["extraction"]["tree"]["GFP"]["np_max"]
        leaf_to_change.add("nuc_est_conv")

        # Regenerate PipelineParameters
        p = Pipeline(PipelineParameters.from_dict(params))

        p.run()
    except Exception as e:
        print(e)
