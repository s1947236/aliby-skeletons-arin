#!/usr/bin/env python3

from aliby.pipeline import PipelineParameters, Pipeline

params = PipelineParameters.default(
    general={
        # "expt_id": "/home/alan/Downloads/19996_ph/",
        "expt_id": 19275,
        "distributed": 6,
        # "directory": "../data/",
        # "directory": "./data/",
        # "tps": 2,
        "overwrite": True,
        "server_info": {
            "host": "***REMOVED***",
            "username": ***REMOVED***,
            "password": "***REMOVED***",
        },
    }
)
p = Pipeline(params)

p.run()
