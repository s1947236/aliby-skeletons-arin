#!/usr/bin/env python3

import h5py

from postprocessor.core.processor import PostProcessorParameters, PostProcessor

from pathlib import Path

fpath = Path(
    # "/home/alan/Documents/dev/skeletons/scripts/data/19129_2020_09_06_DownUpshift_2_0_2_glu_ura_mig1msn2_phluorin_00/ura7_010.h5"
    # "/home/alan/Documents/dev/skeletons/scripts/data/20419_2021_11_02_dose_response_raf_05_075_2_glu_005_2_constantMedia_00"
    # "/home/alan/Documents/dev/libs/aliby/data/sofia/2021_08_24_2Raf_00/2021_08_24_2Raf_00/pic2_001.h5"
    # "/home/alan/Documents/dev/skeletons/data/2021_08_24_2Raf_00/2021_08_24_2Raf_00/pic2_001.h5"
    # "/home/alan/Documents/dev/skeletons/data/2019_07_16_aggregates_CTP_switch_2_0glu_0_0glu_URA7young_URA8young_URA8old_01/2019_07_16_aggregates_CTP_switch_2_0glu_0_0glu_URA7young_URA8young_URA8old_01/URA8_old007.h5"
    # "/home/alan/Documents/dev/skeletons/data/2021_02_23_gluStarv_2_0_2_dual_phl__mig1_msn2_ura7_ura8__wt_00/2021_02_23_gluStarv_2_0_2_dual_phl__mig1_msn2_ura7_ura8__wt_00/phl_mig1_001.h5"
    # "/home/alan/Documents/dev/skeletons/data/19333_2020_11_02_downUpshift_2_0_2_glu_ura7_ura7ha_ura7hr_00/ura7_001.h5"
    "/home/alan/Documents/dev/skeletons/scripts/data/19855_2021_03_20_sugarShift_pal_glu_pal_Myo1Whi5_Sfp1Nhp6a__00/myo1_whi5_001.h5"
)

with h5py.File(fpath, "a") as f:
    if "postprocessing" in f:
        del f["/postprocessing"]
    if "modifiers" in f:
        del f["/modifiers"]

params = PostProcessorParameters.default()
pp = PostProcessor(fpath, params)
pp.run()
