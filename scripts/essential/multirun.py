from aliby.pipeline import PipelineParameters, Pipeline

ids = [
    # Arin
    19972,
    19979,
    20016,
    20212,
    23174,
    25681,
    26643,
    27753,
    27895,
    27917,
    27928,
    27993,
    28018,
    28041,
    28101,
]
for i in ids:
    print(i)
    try:
        params = PipelineParameters.default(
            general={
                "expt_id": i,
                # "tps": 20,
                # "directory": "av_chan",
                "distributed": 11,
                "earlystop": dict(
                    min_tp=100,
                    thresh_pos_clogged=0.4,
                    thresh_trap_ncells=8,
                    ntps_to_eval=5,
                    thresh_trap_area=0.9,
                ),
                # "override_meta": True,
                "overwrite": True,
                "server_info": {
                    "host": "***REMOVED***",
                    "username": ***REMOVED***,
                    "password": "***REMOVED***",
                },
            },
            tiler={"tile_size": 117},
        )
        p = Pipeline(params)
        p.run()
    except Exception as e:
        raise (e)
