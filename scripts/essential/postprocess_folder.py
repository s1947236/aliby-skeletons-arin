#!/usr/bin/env python3
# Post process a single file
from pathlib import Path
import h5py
from postprocessor.core.processor import PostProcessorParameters, PostProcessor
from pathos.multiprocessing import Pool

folder = Path(
    # "/home/alan/Documents/dev/skeletons/scripts/data/20419_2021_11_02_dose_response_raf_05_075_2_glu_005_2_constantMedia_00"
    "/home/alan/Documents/dev/skeletons/scripts/data/19855_2021_03_20_sugarShift_pal_glu_pal_Myo1Whi5_Sfp1Nhp6a__00/"
)


def process_file(filepath):
    # try: # Outcomment this for handling
    # for filepath in Path(folder).rglob("*.h5"):
    with h5py.File(filepath, "a") as f:
        if "postprocessing" in f:
            del f["/postprocessing"]
        if "modifiers" in f:
            del f["/modifiers"]

        params = PostProcessorParameters.default()
        pp = PostProcessor(filepath, params)
        pp.run()


pool = 8
if pool:
    with Pool(10) as p:
        results = p.map(lambda x: process_file(x), Path(folder).rglob("*.h5"))
else:
    for file in Path(folder).glob("*.h5"):
        process_file(file)
