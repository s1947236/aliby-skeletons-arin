#!/usr/bin/env python3
# Fetch the a z-stack for an experimetn
# Developed for Francois on 2022/07/21
# We get a z-stack, wait more than 10 minutes and get the next one.

import numpy as np
from aliby.io.image import Image
from aliby.io.omero import get_data_lazy
from aliby.tile.tiler import Tiler, TilerParameters

with Image(
    109024,
    **{
        "host": "***REMOVED***",
        "username": ***REMOVED***,
        "password": "***REMOVED***",
    },
) as image:
    tiler = Tiler.from_image(image, TilerParameters.default())
    # print("First connection: {}".format(tiler._image.conn.isConnected()))
    # ValueError: Expected one of [distributed, multiprocessing, processes, single-threaded, sync, synchronous, threading, threads]
    sched = "synchronous"  # "threads" or "processes" fail when fethcing omero datasets.
    tc = tiler.get_tc(0, 0)  # .compute(scheduler=sched)

import skimage

skimage.io.imsave("test.tiff", tc)
