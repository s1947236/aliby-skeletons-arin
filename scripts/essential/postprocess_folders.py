#!/usr/bin/env python3
# Post process a single file
from pathlib import Path
import h5py
from postprocessor.core.processor import PostProcessorParameters, PostProcessor
from agora.io.signal import Signal

from pathos.multiprocessing import Pool

folders = [
    # "/home/alan/Documents/dev/stoa_libs/pipeline-core/data/2021_11_04_doseResponse_raf_1_15_2_glu_01_2_dual_phluorin_whi5_constantMedia_00/2021_11_04_doseResponse_raf_1_15_2_glu_01_2_dual_phluorin_whi5_constantMedia_00/",
    # "/home/alan/Documents/dev/stoa_libs/pipeline-core/data/2021_11_24_Bub1_arrest_test_00",
    # "/home/alan/Documents/dev/stoa_libs/pipeline-core/data/Bub1_arrest_test_bak/2021_11_24_Bub1_arrest_test_00",
    # "/home/alan/Documents/dev/skeletons/data/2021_09_15_1_Raf_06/2021_09_15_1_Raf_06/",
    # "/home/alan/Documents/dev/skeletons/data/2021_08_24_2Raf_00/2021_08_24_2Raf_00/",
    # "/home/alan/Documents/dev/skeletons/data/2021_11_01_01_Raf_00/2021_11_01_01_Raf_00/",
]

mother_folder = "/home/alan/Documents/dev/skeletons/scripts/aggregates_exploration/"
folders = list(Path(mother_folder).glob("*"))


def process_file(filepath):
    try:
        # for folder in folders:
        #     for filepath in Path(folder).glob("*.h5"):
        with h5py.File(filepath, "a") as f:
            if "postprocessing" in f:
                del f["/postprocessing"]
            if "modifiers" in f:
                del f["/modifiers"]

            params = PostProcessorParameters.default()
            pp = PostProcessor(filepath, params)
            pp.run()

    except Exception as e:
        print(filepath, " failed")
        print(e)


with Pool(10) as p:
    results = p.map(
        lambda x: process_file(x),
        [file for folder in folders for file in Path(folder).glob("*.h5")],
    )
