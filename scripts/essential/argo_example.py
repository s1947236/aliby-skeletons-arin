from aliby.utils.argo import Argo

# +
# Load from a specific date for faster results
argo = Argo(
    min_date=(2020, 1, 1),
    host="***REMOVED***",
    user=***REMOVED***,
    password="***REMOVED***",
)
argo.load()
# -

argo.tags("Alan")


# You can get the available datasets and check whether they were successful
# complete = argo.return_completed(kind="complete")
# print(complete)

# Or correctly interrupted
# interrupted = argo.return_completed(kind="interrupted")
# print(interrupted)

# Also you can count the number of logs
# tmp = argo.count_in_log(kind="errors")
# alerts = argo.count_in_log(kind="drift_alert")
