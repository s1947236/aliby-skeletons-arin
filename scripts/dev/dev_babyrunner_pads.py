#!/usr/bin/env jupyter
"""
Development script to to improve baby's tracker to scale better with more crowded images.
"""

from pathlib import Path

from dask.array.image import imread
from aliby.baby_client import BabyParameters, BabyRunner
from aliby.tile.tiler import TilerParameters, Tiler
from aliby.io.image import ImageDirectory

dpath = Path("/home/alan/Documents/dev/lucias_data/exp_00/pos001/")

channel_paths = {
    "DIC": str(dpath / "CutDIC/*.png"),
    # "Citrine": str(dpath / "CutCitrine/*.png"),
    # "Dye": str(dpath / "CutDye/*.png"),
    "Segmentation": str(dpath / "Segmentation/Components/*.tif"),
}
imd = ImageDirectory(channel_paths)

tiler_params = TilerParameters.default().to_dict()
for k, v in {"ref_channel": "DIC", "tile_size": None}.items():
    tiler_params[k] = v

# Load Tiler
tiler = Tiler.from_image(imd, TilerParameters.from_dict(tiler_params))


# Load BabyRunner to give Tracker access to Tiler
br = BabyRunner.from_tiler(BabyParameters.default(n_stacks="1z"), tiler)

# Try to get babyrunner working for pads
X = tiler.get_tc(0, 0)
np.moveaxis(np.stack((X,)), 0, -1).shape
self = br.brain
br.brain.morph_predict(X)

# Load Lucia's outline dataset
plt.imshow(image[0])
plt.savefig("test.png", dpi=300)
# Select independent outlines
# Benchmark speed
# Bootstrap different regions
# Repeat

import matplotlib.pyplot as plt
