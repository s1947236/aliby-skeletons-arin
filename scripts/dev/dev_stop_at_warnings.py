#!/usr/bin/env python3
# Script that stops at warnings and asks if we want to stop there. Used for warning clean-up
import warnings


from aliby.pipeline import PipelineParameters, Pipeline


def handle_warning(message, category, filename, lineno, file=None, line=None):
    print("A warning occurred:")
    print(message)
    print("Do you wish to continue?")

    while True:
        response = input("y/n: ").lower()
        if response not in {"y", "n"}:
            print("Not understood.")
        else:
            break

    if response == "n":
        raise category(message)


# warnings.showwarning = handle_warning

warnings.filterwarnings(
    "ignore",
    message="Trying to unpickle estimator RandomForestClassifier from version 0.22.2 when using version 1.0.2. This might lead to breaking code or invalid results. Use at your own risk. For more info please refer to: https://scikit-learn.org/stable/modules/model_persistence.html#security-maintainability-limitations",
)


def pause_if_warning(function, *args, **kwargs):
    with warnings.catch_warnings():
        warnings.showwarning = handle_warning
        return function(*args, **kwargs)


params = PipelineParameters.default(
    general={
        # "expt_id": 20419,
        "expt_id": 19996,
        # "expt_id": 20422,
        "distributed": 2,
        "tps": 5,
        "directory": "new1",
        "server_info": {
            "host": "***REMOVED***",
            "username": ***REMOVED***,
            "password": "***REMOVED***",
        },
        # "filter": ["glu"],
    }
)
p = Pipeline(params)

pause_if_warning(p.run())
