#!/usr/bin/env python3
from pathlib import Path
from aliby.io.signal import Signal

fpath = Path(
    # "/home/alan/Documents/dev/stoa_libs/pipeline-core/data/2021_11_04_doseResponse_raf_1_15_2_glu_01_2_dual_phluorin_whi5_constantMedia_00/glu_01_015.h5"
    "/home/alan/Documents/dev/stoa_libs/pipeline-core/data/2021_06_27_downUpShift_2_0_2_glu_dual_phl_ura8__00/2021_06_27_downUpShift_2_0_2_glu_dual_phl_ura8__00/phl_ura8_005.h5"
)
s = Signal(fpath)

# tmp = s["postprocessing/births/extraction_general_None_volume"]
tmp = s["extraction/general/None/area"]
tmp = s.get_raw("extraction/general/None/area")
