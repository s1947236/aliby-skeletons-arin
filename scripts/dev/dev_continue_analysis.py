#!/usr/bin/env python3
# Script to test interrupted analysis continuation

# fpath = "/home/alan/Documents/dev/skeletons/data/2019_07_16_aggregates_CTP_switch_2_0glu_0_0glu_URA7young_URA8young_URA8old_01/2019_07_16_aggregates_CTP_switch_2_0glu_0_0glu_URA7young_URA8young_URA8old_01/URA7_young001.h5"
fpath = "/home/alan/Documents/dev/skeletons/data/2021_06_27_ph_calibration_dual_phl_ura8_5_04_5_83_7_69_7_13_6_59__01/2021_06_27_ph_calibration_dual_phl_ura8_5_04_5_83_7_69_7_13_6_59__01/ph_5_04_001.h5"

from aliby.pipeline import PipelineParameters, Pipeline

params = PipelineParameters.default(
    general={
        # "expt_id": 16543,
        "expt_id": 19996,
        "distributed": 0,
        "tps": 4,
        "filter": "ph_5_04_001",
        # "directory": "../cont",
        "server_info": {
            "host": "***REMOVED***",
            "username": ***REMOVED***,
            "password": "***REMOVED***",
        },
    }
)

yaml = params.to_yaml("params.yml")

import h5py
from aliby.experiment import MetaData

try:
    meta = MetaData(None, fpath)
    meta.add_fields({"parameters_yaml": yaml})
except:
    pass

delete = False  # Start debugging seg/extraction writing here


import os

if delete:

    try:
        os.remove(fpath)
    except:
        pass

    pipeline = Pipeline(params)
else:
    with h5py.File(fpath, "r") as f:
        params = PipelineParameters.from_yaml(f.attrs["parameters_yaml"])
pipeline = Pipeline(params)
pipeline.run()

fpath_cont1 = "/home/alan/Documents/dev/skeletons/data/2021_06_27_ph_calibration_dual_phl_ura8_5_04_5_83_7_69_7_13_6_59__01/cont.h5"
fpath_int1 = "/home/alan/Documents/dev/skeletons/data/2021_06_27_ph_calibration_dual_phl_ura8_5_04_5_83_7_69_7_13_6_59__01/break.h5"
# with h5py.File(fpath_int1) as f:
#     print(f["trap_info/trap_locations"])

from agora.io.signal import Signal

# from agora.io.cells import Cells

s = Signal(fpath_cont1)
s2 = Signal(fpath_int1)

# # c = Cells.from_source(fpath_cont1)
# # c2 = Cells.from_source(fpath_int1)
# import pickle as pkl
# from pathlib import Path

# pickdir = Path(
#     "/home/alan/Documents/dev/skeletons/data/2021_06_27_ph_calibration_dual_phl_ura8_5_04_5_83_7_69_7_13_6_59__01/2021_06_27_ph_calibration_dual_phl_ura8_5_04_5_83_7_69_7_13_6_59__01/"
# )
# n = 1
# with open(pickdir / f"{n}_live_state.pkl", "rb") as f:
#     live = pkl.load(f)

# with open(pickdir / f"{n}_read_state.pkl", "rb") as f:
#     read = pkl.load(f)

# from agora.io.reader import StateReader

# raw = StateReader(pickdir / "ph_5_04_001.h5")
# all_data = raw.read_all()

# import numpy as np

# for k in read[0].keys():  # Check State I/O is the same
#     print(k)
#     for b, (r, l) in enumerate(zip(read, live)):
#         if k == "prev_feats" or k == "cell_lbls":
#             print(np.all([np.isclose(i, j).all() for i, j in zip(r[k], l[k])]))

#         elif k in ("lifetime", "p_was_mother", "p_was_bud", "p_is_mother"):
#             print(np.isclose(r[k], l[k]).all())
#         elif k == "ba_cum":
#             continue
#         else:
#             print(np.all([r[k] == l[k] for r, l in zip(read, live)]))
