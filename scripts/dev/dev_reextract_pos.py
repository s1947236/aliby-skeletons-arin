#!/usr/bin/env python3

"""
Reextract a single position with new parameters.
"""
from aliby.pipeline import PipelineParameters, Pipeline


fpath = "/home/alan/Documents/dev/data_analysis/ph_msn2/phluorin_msn2_005.h5"

pp = Pipeline.from_existing_h5(fpath)
pp.parameters.general["distributed"] = 0  # for debugging
pp.parameters.general["overwrite"] = "extraction"
pp.parameters.general["tps"] = 5
pp.run()
