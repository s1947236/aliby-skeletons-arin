#!/usr/bin/env python3

from pathlib import Path
import h5py

fpath = Path(
    "/home/alan/Documents/dev/stoa_libs/pipeline-core/data/2021_11_24_Bub1_arrest_test_00/2021_11_24_Bub1_arrest_test_00/Bub2D_013.h5"
)
with h5py.File(fpath) as f:

    ma = f["cell_info/mother_assign"][()]
    mad = f["cell_info/mother_assign_dynamic"][()]
    label = f["cell_info/cell_label"][()]
    trap = f["cell_info/trap"][()]
    timepoint = f["cell_info/timepoint"][()]
