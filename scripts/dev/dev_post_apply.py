#!/usr/bin/env python3
# Test pre_post on postprocessed datasets

#!/usr/bin/env python3

import h5py


from pathlib import Path

fpath = Path(
    "/home/alan/Documents/dev/skeletons/data/2021_11_02_dose_response_raf_05_075_2_glu_005_2_constantMedia_00/2021_11_02_dose_response_raf_05_075_2_glu_005_2_constantMedia_00/raf05_s12.h5"
)
from agora.io.signal import Signal

s = Signal(fpath)
s.datasets
df = s["postprocessing/dsignal/extraction_general_None_volume"]
