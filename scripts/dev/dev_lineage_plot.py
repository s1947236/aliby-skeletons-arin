#!/usr/bin/env python3
# Produces mergerusage and pickerusage pngs of Alan's thesis

import h5py
from postprocessor.core.processor import PostProcessorParameters, PostProcessor

from pathlib import Path

fpath = Path(
    # "/home/alan/Documents/dev/stoa_libs/pipeline-core/data/2021_11_04_doseResponse_raf_1_15_2_glu_01_2_dual_phluorin_whi5_constantMedia_00/glu_01_015.h5"
    "/home/alan/Documents/dev/stoa_libs/pipeline-core/data/2021_11_04_doseResponse_raf_1_15_2_glu_01_2_dual_phluorin_whi5_constantMedia_00/raf_1_004.h5"
)

with h5py.File(fpath, "a") as f:
    if "postprocessing" in f:
        del f["/postprocessing"]
    if "modifiers" in f:
        del f["/modifiers"]

params = PostProcessorParameters.default().to_dict()
pp = PostProcessor(fpath, params)
pp.run()

from pathlib import Path
from aliby.io.signal import Signal

s = Signal(fpath)

tmp = s["postprocessing/bud_metric/extraction_general_None_volume"]

import seaborn as sns
import matplotlib.pyplot as plt


sns.set_style("darkgrid")
sns.set_context("talk")
trap_id = 6
fig, axes = plt.subplots(1, 2)
raw = s.get_raw("extraction/general/None/volume")
proc = s["extraction/general/None/volume"]
trap = proc.index.get_level_values(0).unique()[trap_id]
print(trap)
sns.heatmap(
    raw.loc[trap].sort_index(),
    robust=True,
    ax=axes[0],
    cbar_kws={"label": r"Volume ($\mu m^{3}$)"},
)
sns.heatmap(
    proc.loc[trap].sort_index(),
    robust=True,
    ax=axes[1],
    cbar_kws={"label": r"Volume ($\mu m^{3}$)"},
)

axes[0].set_title("Before post-processing")
axes[1].set_title("After post-processing")
axes[0].set_xlabel("Time point")
axes[1].set_xlabel("Time point")
axes[0].set_ylabel("Cell Label")
axes[1].set_ylabel("Cell Label")
plt.show()
