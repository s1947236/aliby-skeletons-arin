#!/usr/bin/env jupyter
"""
Development script to to improve baby's tracker to scale better with more crowded images.
"""

from pathlib import Path

from dask.array.image import imread
from aliby.baby_client import BabyParameters, BabyRunner
from aliby.tile.tiler import TilerParameters, Tiler
from aliby.io.image import ImageDirectory

dpath = Path("/home/alan/Documents/dev/lucias_data/exp_00/pos001/")

channel_paths = {
    "DIC": str(dpath / "CutDIC/*.png"),
    # "Citrine": str(dpath / "CutCitrine/*.png"),
    # "Dye": str(dpath / "CutDye/*.png"),
    "Segmentation": str(dpath / "Segmentation/Components/*.tif"),
}

imd = ImageDirectory(channel_paths)

tiler_params = TilerParameters.default().to_dict()
for k, v in {"ref_channel": "DIC", "tile_size": None}.items():
    tiler_params[k] = v

# Load Tiler
tiler = Tiler.from_image(imd, TilerParameters.from_dict(tiler_params))

# Try to get babyrunner working for pads
X1 = tiler.get_tc(0, 1)[0]
X2 = tiler.get_tc(1, 1)[0]
# 13um pixel size


from baby.tracker.core import CellTracker

# tmp1 = ct.calc_feats_from_mask(X1)
# nonscaled = ct.calc_feats_from_mask(X1, scale=False)
# scaled = ct.calc_feats_from_mask(X1, scale=True)
a = list()
ct = CellTracker(pixel_size=0.13)
# print(probs)
# a.append([size, probs.diagonal().sum() - np.fliplr(probs).diagonal().sum()])
tp = 500
# Section to time tracking time based on number of cells

from time import perf_counter

from tqdm import tqdm

probs = ct.probabilities_from_impair(X1, X2, kwargs_feat_calc={"scale": True})
for tp in tqdm(range(tiler.shape[0])):
    X1, X2 = [tiler.get_tc(i, 1)[0] for i in range(tp, tp + 2)]
    t = perf_counter()
    # probs = ct.probabilities_from_impair(X1, X2, kwargs_feat_calc={"scale": True})
    elapsed = perf_counter() - t
    log_info_3.append((np.multiply(*probs.shape), elapsed))
