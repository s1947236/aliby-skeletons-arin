#!/usr/bin/env python3


from agora.io.reader import StateReader

fpath = "/home/alan/Documents/dev/skeletons/data/2021_06_27_ph_calibration_dual_phl_ura8_5_04_5_83_7_69_7_13_6_59__01/2021_06_27_ph_calibration_dual_phl_ura8_5_04_5_83_7_69_7_13_6_59__01/ph_5_04_001.h5"
sreader = StateReader(fpath)
state = sreader.get_formatted_states()
