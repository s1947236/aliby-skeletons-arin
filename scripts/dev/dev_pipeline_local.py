#!/usr/bin/env python3
from typing import Union
from pathlib import Path, PosixPath

from aliby.io.dataset import DatasetLocal

dpath = Path("/home/alan/Downloads/19993_pypipeline_unit_test")

dl = DatasetLocal(dpath)

from aliby.pipeline import Pipeline, PipelineParameters

params = PipelineParameters.default(
    general={
        "expt_id": dpath,
        "distributed": 0,
        "directory": "./tmp/",
        "tps": 2,
    }
)
p = Pipeline(params)

p.run()
