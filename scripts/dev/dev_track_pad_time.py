#!/usr/bin/env jupyter
"""
Development script to to improve baby's tracker to scale better with more crowded images.

This script proved that running the classifier prediction is not the bottleneck
"""

import pickle
from pathlib import Path

from aliby.io.image import ImageDirectory
from aliby.tile.tiler import Tiler, TilerParameters
from baby.tracker.core import CellTracker
from tqdm import tqdm

dpath = Path("/home/alan/Documents/dev/data/lucia/")
outpath = Path(
    "/home/alan/Documents/sync/PhD/writing/drafts/thesis/chapter1_tracker/img"
)

channel_paths = {
    "DIC": str(dpath / "CutDIC/*.png"),
    # "Citrine": str(dpath / "CutCitrine/*.png"), # Outcommented because they don't hve the same tps
    # "Dye": str(dpath / "CutDye/*.png"),
    "Segmentation": str(dpath / "Segmentation/Components/*.tif"),
}

# Load Tiler to easily fetch images
tiler_params = TilerParameters.default().to_dict()
for k, v in {"ref_channel": "DIC", "tile_size": None}.items():
    tiler_params[k] = v

tiler = Tiler.from_image(
    ImageDirectory(channel_paths), TilerParameters.from_dict(tiler_params)
)

# 13um pixel size in x100 (I presume). I tested different sizes and this did best
ct = CellTracker(pixel_size=0.125)
from time import perf_counter
import numpy as np

ntps = tiler.shape[0]
compared_times = np.empty((ntps, 2))
max_distance = (
    3  # Lower values improve speed, but if it is too low it can generate many new cells
)
state1 = None
state2 = None
for tp in tqdm(range(ntps)):
    snapshot = tiler.get_tc(tp, 1)[0]
    t = perf_counter()
    labels1, state1 = ct.run_tp(snapshot, state=state1)
    t1 = perf_counter() - t

    t = perf_counter()
    labels2, state2 = ct.run_tp(snapshot, state=state2, max_distance=max_distance)
    t2 = perf_counter() - t
    compared_times[tp] = (t1, t2)

with open("time_comparison.pkl", "wb") as f:
    pickle.dump(compared_times, f)

import pandas as pd
import seaborn as sns

sns.set_style("darkgrid")
df = pd.DataFrame(
    compared_times, columns=("raw", "max_distance={} um".format(max_distance))
)
melted = df.melt(var_name="Heuristic", value_name="Time (seconds)")
melted["tp"] = np.tile(range(len(df)), 2)
melted["n_cells"] = np.tile([len(x) for x in state1["cell_lbls"]], 2)

import seaborn as sns

sns.scatterplot(
    data=melted,
    x="n_cells",
    y="Time (seconds)",
    hue="Heuristic",
    # style="Heurisic",
    alpha=0.8,
)
import matplotlib.pyplot as plt

plt.xlabel("Number of cells")
plt.savefig(outpath / "tracking_time_scaling.png", dpi=300)

print(state1["max_lbl"], state2["max_lbl"])
