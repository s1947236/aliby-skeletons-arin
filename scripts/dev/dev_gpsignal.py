#!/usr/bin/env python3

import pandas as pd
from postprocessor.core.processes.gpsignal import gpsignal, gpsignalParameters

df = pd.DataFrame(
    [[1 + (i * j) for i in range(10)] for j in range(3)],
    index=pd.MultiIndex.from_arrays([(0, 0, 1), (1, 2, 1)]),
)


gpsig = gpsignal(gpsignalParameters.default())
dfs = gpsig.run(df)
