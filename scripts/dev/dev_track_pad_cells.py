#!/usr/bin/env jupyter
"""
Development script to to improve baby's tracker to scale better with more crowded images.
"""

import pickle
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from aliby.io.image import ImageDirectory
from aliby.tile.tiler import Tiler, TilerParameters
from baby.tracker.core import CellTracker
from tqdm import tqdm

dpath = Path("/home/alan/Documents/dev/data/lucia/")
outpath = Path(
    "/home/alan/Documents/sync/PhD/writing/drafts/thesis/chapter1_tracker/figs"
)

channel_paths = {
    "DIC": str(dpath / "CutDIC/*.png"),
    # "Citrine": str(dpath / "CutCitrine/*.png"), # Outcommented because they don't hve the same ntps
    # "Dye": str(dpath / "CutDye/*.png"),
    "Segmentation": str(dpath / "Segmentation/Components/*.tif"),
}

# Load Tiler to easily fetch images
tiler_params = TilerParameters.default().to_dict()
for k, v in {"ref_channel": "DIC", "tile_size": None}.items():
    tiler_params[k] = v

tiler = Tiler.from_image(
    ImageDirectory(channel_paths), TilerParameters.from_dict(tiler_params)
)


# Instatiate tracker
ct = CellTracker(pixel_size=0.13)

# Try reading from file
try:
    fname = "baby_tracking_labels.pkl"
    with open(fname, "rb") as f:
        all_labels = pickle.load(f)
except:
    # 13um pixel size in x100 (I presume). I tested different sizes and this did best
    all_labels = []
    state = None
    for tp in tqdm(range(tiler.shape[0])):  # Iteratively run CellTracker
        snapshot = tiler.get_tc(tp, 1)[0]
        new_labels, state = ct.run_tp(snapshot, state=state)
        all_labels.append(new_labels)

    with open(fname, "wb") as f:
        pickle.dump(all_labels, f)


# Plot the probabilities of a time-point in the middle of our time-lapse
tp = tiler.shape[0] // 2


probs = ct.probabilities_from_impair(*[tiler.get_tc(i, 1)[0] for i in (tp, tp + 1)])
sns.heatmap(probs)
plt.title("Prob matrix comparing t = {} and {}".format(tp - 1, tp))
plt.tight_layout()
plt.savefig(outpath / "tracking_probability_matrix.png", dpi=500)
plt.close()


import string

# Confirm visually that Baby's CellTracker does improve tracking when compared to raw output
import matplotlib.pyplot as plt
import numpy as np
from skimage.measure import regionprops_table

X1, X2 = [tiler.get_tc(i, 1)[0] for i in (tp, tp + 1)]

rp_1 = regionprops_table(X1, properties=("centroid", "label"))
rp_2 = regionprops_table(X2, properties=("centroid", "label"))


# Get BABY's CellTracker predictions
prev_labels = list(range(1, int(X1.max() + 1)))
labels, _, _ = ct.get_new_lbls(
    X2, [prev_labels], [ct.calc_feats_from_mask(X1)], X1.max()
)

X1 = X1.astype(float)
X2 = X2.astype(float)
X1[X1 == 0] = np.nan
X2[X2 == 0] = np.nan

plt.imshow(
    np.concatenate((X2, X1, X2), axis=1),
    cmap=sns.color_palette(
        "Paired",
        as_cmap=True,
    ),
    interpolation=None,
    interpolation_stage="rgba",
)

text = string.printable[:20]
symbols = text * ((max(labels) // len(text)) + 1)
for i in range(int(max(np.nanmax(X1), np.nanmax(X2), max(labels)) - 1)):
    for j, rp in enumerate((rp_2, rp_1)):
        try:
            xloc = rp["centroid-1"][i]
            yloc = rp["centroid-0"][i]
            plt.text(xloc + X2.shape[1] * j, yloc, symbols[i], fontsize=1)
            if j == 0 and labels[i] in prev_labels:
                plt.text(
                    xloc + X1.shape[1] + X2.shape[1],
                    yloc,
                    symbols[labels[i] - 1],
                    fontsize=1,
                )

        except Exception as e:
            print(e)

plt.ylim(400, 800)
plt.xlabel("Raw ids, Original ids, and Tracked ids")
plt.title("BABY's CellTracker seems to perform well in pads")
plt.savefig(outpath / "tracking_visual_confirmation.png", dpi=600)
plt.close()

# Plot histogram with cell ids during last time-point
sns.histplot(all_labels[-1])
plt.xlabel("Unique cell label")
plt.savefig(outpath / "histogram_last_tp.png", dpi=300)
plt.close()
