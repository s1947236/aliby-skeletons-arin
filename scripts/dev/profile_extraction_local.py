#!/usr/bin/env python3
"""
Use it to time specific extraction metrics, the example given tests nuc_conv_3d.


"""

from extraction.core.extractor import Extractor, ExtractorParameters

fpath = "/home/alan/Documents/dev/skeletons/scripts/data/19447_2020_11_18_downUpshift_2_0_2_glu_gcd2_gcd6_gcd7__02/gcd2_001.h5"

# import h5py

# with h5py.File(fpath, "r") as f:
#     parameters = yaml.safe_load(f.attrs["parameters"])
#     image_id = f.attrs["image_id"]
# extraction_parameters = parameters["extraction"]


from agora.io.bridge import image_creds_from_h5, parameters_from_h5


from aliby.io.omero import Image
from aliby.tile.tiler import Tiler, TilerParameters

image_id, creds = image_creds_from_h5(fpath)

with Image(image_id, **creds) as image:

    params = parameters_from_h5(fpath)
    params["extraction"]["tree"]["Flavin"]["None"] = ["nuc_conv_3d"]

    import cProfile
    import pstats

    ext = Extractor.from_tiler(
        parameters=ExtractorParameters.from_dict(params["extraction"]),
        store=fpath,
        tiler=Tiler.from_hdf5(image, fpath, TilerParameters.from_dict(params["tiler"])),
    )
    pr = cProfile.Profile()
    pr.enable()
    tmp = ext.run(tps=[0])

    pr.disable()
    ps = pstats.Stats(pr)
    ps.dump_stats("speed.prof")
