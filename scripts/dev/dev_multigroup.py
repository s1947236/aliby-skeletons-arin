#!/usr/bin/env python3
import seaborn as sns
import matplotlib.pyplot as plt

from aliby.grouper import MultiGrouper

fpath = "/home/alan/Documents/dev/skeletons/scripts/aggregates_exploration/"

mg = MultiGrouper(fpath)

# Plot a table with the number of signals per experiment
# tmp = mg.sigtable_plot()

from agora.io.signal import Signal

# agg = mg.aggregate_signal("extraction/general/None/volume")
gr = mg.aggregate_signal(
    "/postprocessing/bud_metric/postprocessing_dsignal_postprocessing_savgol_extraction_general_None_volume"
)
tidy = gr.melt(ignore_index=False).reset_index()
sns.lineplot(
    data=tidy.dropna(), x="variable", y="value", hue="group", style="experiment"
)
# s = mg.groupers[0].signals["URA7_016"]
# tmp = s["extraction/general/None/area"]
