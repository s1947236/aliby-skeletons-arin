#!/usr/bin/env python3


from pathlib import Path

from aliby.baby_client import BabyRunner, BabyParameters

fpath = Path("/home/alan/Documents/dev/lucias_data/test/")


# Develop tiler for folder

from aliby.io.image import ImageLocal

il = ImageLocal(fpath / "pos001.tiff", dimorder="TXY")

img = il.get_data_lazy_local()
# First sort the existing elements, place them at the end.
# Then go left to right, if it is in the correct position

bp = BabyParameters.default().to_dict()
bp["model_config"] = modelset
# with ImageLocal(fpath / "pos001.tiff", dimorder="TXY") as image:
#     br = BabyRunner.from_tiler(BabyParameters.from_dict(bp), None)

# fpath = "/home/alan/Documents/dev/libs/baby/baby/models/I1_prime_unet_4s_20210302.hdf5"
from tensorflow.keras import models
from aliby.baby_client import choose_model_from_params

# modelset = "evolve_brightfield_60x_1z"

from baby.losses import bce_dice_loss, dice_loss, dice_coeff
from baby import BabyBrain, BabyCrawler, modelsets

modelset = modelsets()["evolve_brightfield_60x_1z"]
modelset = modelsets()["prime95b_brightfield_60x_5z"]
brain = BabyBrain(**modelset)

# tmp = models.load_model(
#     modelsets()["prime95b_brightfield_60x_1z"]['momrph_model_file']

#     custom_objects={
#         "bce_dice_loss": bce_dice_loss,
#         "dice_loss": dice_loss,
#         "dice_coeff": dice_coeff,
#     },
# )
