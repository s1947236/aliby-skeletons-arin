#!/usr/bin/env python3

from copy import copy
from pathlib import Path

from aliby.pipeline import PipelineParameters, Pipeline

import h5py

fpath = "/home/alan/Documents/dev/data_analysis/ph_msn2/phluorin_mig1_026.h5"
pl = Pipeline.from_existing_h5(fpath)
params_d = pl.parameters.to_dict()
params_d["general"]["overwrite"] = "postprocessing"
params_d["general"]["distributed"] = 0  # for debugging
# params_d["general"]["filter"] = ["msn2", "mig1", "ura8"]  # Sort out order

pl = Pipeline(PipelineParameters.from_dict(params_d))
tmp = pl.run()

from postprocessor.core.processes.savgol import savgol  # , savgolParameters
from postprocessor.core.processes.dsignal import dsignal  # , dsignalParameters
from postprocessor.core.processes.findpeaks import findpeaks  # , findpeaksParameters

from agora.io.signal import Signal

s = Signal(fpath)
# df = s.retained("/extraction/")


def find_transitions(df: pd.DataFrame, preprocess=None, direction=None):
    if preprocess is None:
        preprocess = [(f, {}) for f in (savgol, dsignal, findpeaks)]

    if direction is None:
        direction = 1

    data = copy(df)
    for process, parameters in preprocess:
        data = process.as_function(df, *parameters)

    return data
