#!/usr/bin/env python3
# Develop a local tiler, and compare the outputs to make sure they behave the same way
# Tested on experiment 19993 and its downloaded OME-TIFF images

from pathlib import Path
from aliby.io.image import Image, ImageLocal
from aliby.tile.tiler import Tiler, TilerParameters

# Link to an example ome.tif file
fpath = Path("/home/alan/Downloads/19993_pypipeline_unit_test/pos002.ome.tif")


sinfo = dict(host="***REMOVED***", username=***REMOVED***, password="**REMOVED**")


with ImageLocal(fpath) as image:
    t1 = Tiler.from_image(image, TilerParameters.default())
    print(t1.get_tc(0, 0).shape)

with Image(103209, **sinfo) as image:
    t2 = Tiler.from_image(image, TilerParameters.default())
    print(t2.get_tc(0, 0).shape)
