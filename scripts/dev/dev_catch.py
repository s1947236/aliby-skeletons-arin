#!/usr/bin/env python3

from pathlib import Path
import pandas as pd
from catch22 import catch22_all

from aliby.grouper import NameGrouper

path = Path(
    "/home/alan/Documents/dev/stoa_libs/pipeline-core/data/2021_11_04_doseResponse_raf_1_15_2_glu_01_2_dual_phluorin_whi5_constantMedia_00"
)
ng = NameGrouper(path)
whi5 = ng.concat_signal("postprocessing/dsignal/extraction_general_None_volume")
whi5 = ng.concat_signal("postprocessing/dsignal/extraction_general_None_volume")
import seaborn as sns

df = agg_gr[agg_gr.notna().sum(axis=1) > 90]
df = df.apply(lambda x: x.rolling(15).mean(), axis=1)
sns.heatmap(df, robust=True)
plt.show()

adf = whi5.loc[whi5.notna().sum(axis=1) > 90]
# Removing nans is necessary for catch22 to work
tmp = [catch22_all(adf.iloc[i, :].dropna().values) for i in range(len(adf))]
# tmp = [catch22_all(adf.iloc[i, :].values) for i in range(len(adf))]
cdf = pd.DataFrame([t["values"] for t in tmp], index=adf.index, columns=tmp[0]["names"])

from tsman import TS_Normalize

norm = pd.DataFrame(TS_Normalize(cdf.values), index=cdf.index, columns=cdf.columns)

from sklearn import decomposition
from sklearn import datasets

fig = plt.figure(1, figsize=(4, 3))
plt.clf()

plt.cla()
pca = decomposition.PCA(n_components=5)
pca.fit(X)
Y = pca.transform(X)
from mpl_toolkits.mplot3d import Axes3D

ax = Axes3D(fig, rect=[0, 0, 0.95, 1], elev=48, azim=134)

components = pd.DataFrame(Y, index=norm.index)
