#!/usr/bin/env python3

from copy import copy
from pathlib import Path
import traceback

import h5py

from aliby.pipeline import PipelineParameters, Pipeline


fpaths = [
    "/home/alan/Documents/dev/data_analysis/ph_msn2/19129_2020_09_06_DownUpshift_2_0_2_glu_ura_mig1msn2_phluorin_00",
    "/home/alan/Documents/dev/data_analysis/ph_msn2/19144_2020_09_07_DownUpshift_2_0_2_glu_ura_mig1msn2_phluorin_secondRound_00",
    "/home/alan/Documents/dev/data_analysis/ph_msn2/19169_2020_09_09_downUpshift_2_0_2_glu_ura8_phl_mig1_phl_msn2_03",
    "/home/alan/Documents/dev/data_analysis/ph_msn2/19232_2020_10_02_downUpshift_twice_2_0_2_glu_ura8_phluorinMsn2_phluorinMig1_01",
]
for fpath in fpaths:
    try:
        pl = Pipeline.from_folder(fpath)
        params_d = pl.parameters.to_dict()
        params_d["general"]["overwrite"] = "postprocessing"
        params_d["general"]["distributed"] = 8  # 0 for debugging
        params_d["general"]["filter"] = ["msn2", "mig1", "ura8"]  # Sort out order

        pl = Pipeline(PipelineParameters.from_dict(params_d))
        tmp = pl.run()

    except Exception as e:
        print(e)
        traceback.print_exc()
