#!/usr/bin/env python3

from postprocessor.grouper import NameGrouper
from postprocessor.compiler import ExperimentCompiler

fpath = "/home/alan/Documents/dev/skeletons/data/2020_10_23_downUpshift_2_0_2_glu_dual_phluorin__glt1_psa1_ura7__twice__04/2020_10_23_downUpshift_2_0_2_glu_dual_phluorin__glt1_psa1_ura7__twice__04"

compiler = ExperimentCompiler(None, fpath)
compilation = compiler.run()

self = compiler


def compile_aggregated(
    self, signals=None, tp=None, metrics=None, mode=None, *args, **kwargs
) -> pd.DataFrame:

    if signals == None:
        signals = [
            "postprocessing/births/extraction_general_None_volume",
            "extraction/mCherry/np_max/max2p5pc",
            "extraction/GFPFast/np_max/median",
        ]

    if tp == None:
        tp = 0

    if metrics == None:
        metrics = ("max", "mean", "median", "count", "std", "sem")

    if mode == None:
        mode = True

    df = pd.concat(
        [
            getattr(
                self.concat_signal(sigloc=sig, mode=mode, *args, **kwargs),
                met,
            )(axis=1)
            for met in metrics
            for sig in signals
        ],
        axis=1,
    )

    df.columns = ["_".join((sig, met)) for met in metrics for sig in signals]

    return df


import pandas as pd

# c = compile_aggregated(compiler)
mini = compile_aggregated.concat_signal(
    compiler, "postprocessing/births/extraction_general_None_volume"
)
