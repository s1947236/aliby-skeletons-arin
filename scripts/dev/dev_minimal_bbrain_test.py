#!/usr/bin/env python3


from baby.losses import bce_dice_loss, dice_loss, dice_coeff
from baby import BabyBrain, BabyCrawler, modelsets

modelset = modelsets()["prime95b_brightfield_60x_5z"]
brain = BabyBrain(**modelset)
