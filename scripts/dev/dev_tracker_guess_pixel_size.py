#!/usr/bin/env jupyter
"""
Script used to guess pixel size in new data when knowing the labels of a single image
"""

import pickle
from pathlib import Path

from aliby.io.image import ImageDirectory
from aliby.tile.tiler import Tiler, TilerParameters
from baby.tracker.core import CellTracker
from tqdm import tqdm

dpath = Path("/home/alan/Documents/dev/lucias_data/exp_00/pos001/")

channel_paths = {
    "DIC": str(dpath / "CutDIC/*.png"),
    # "Citrine": str(dpath / "CutCitrine/*.png"), # Outcommented because they don't hve the same tps
    # "Dye": str(dpath / "CutDye/*.png"),
    "Segmentation": str(dpath / "Segmentation/Components/*.tif"),
}

# Load Tiler to easily fetch images
tiler_params = TilerParameters.default().to_dict()
for k, v in {"ref_channel": "DIC", "tile_size": None}.items():
    tiler_params[k] = v

tiler = Tiler.from_image(
    ImageDirectory(channel_paths), TilerParameters.from_dict(tiler_params)
)

X1, X2 = [tiler.get_tc(i, 1)[0] for i in range(2)]

import numpy as np

n = 1000
probs = np.empty((n, 2, 2))
for i, size in enumerate(np.linspace(0.001, 1, n)):
    ct = CellTracker(pixel_size=size)
    probs[i] = ct.probabilities_from_impair(X1, X2)


iden = np.identity(2, dtype=bool)
g = np.array([x[iden].sum() - x[np.fliplr(iden)].sum() for x in probs])

plt.scatter(range(len(g)), g, alpha=0.5)
print(list(np.linspace(0.001, 1, n))[g.argmax()])
# We thus presume 0.1625 (80x) is the lens zoom
