#!/usr/bin/env python3
"""
Helper script to develop and debug individual postprocesses
"""
from pydoc import locate
from pathlib import Path

import numpy as np
import h5py

from agora.io.signal import Signal
from agora.utils.lineage import mb_array_to_dict
from postprocessor.core.processor import PostProcessorParameters, PostProcessor


fpath = Path(
    "/home/alan/Documents/dev/skeletons/data/19333_2020_11_02_downUpshift_2_0_2_glu_ura7_ura7ha_ura7hr_00/ura7_001.h5"
)


s = Signal(fpath)

process = "bud_metric"
process_path = ".".join(("postprocessor.core.processes", process, process))


process = locate(process_path)
parameters = locate(process_path + "Parameters")
df = s["/extraction/general/None/volume"]
loaded_process = process(parameters.default())
with h5py.File(fpath, "r") as f:
    lineage = np.vstack(
        [
            f[f"postprocessing/lineage_merged/{k}"]
            for k in ("trap", "mother_label", "daughter_label")
        ]
    ).T
loaded_process.lineage = lineage
loaded_process.run(df)
