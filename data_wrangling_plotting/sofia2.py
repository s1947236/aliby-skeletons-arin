#!/usr/bin/env python3


from pathlib import Path
from pcore.grouper import NameGrouper

folder = Path(
    "/home/alan/Documents/dev/stoa_libs/pipeline-core/data/2021_08_24_2Raf_00/2021_08_24_2Raf_00/tmp"
)
g = NameGrouper(folder)

# Aggretated volumes
vols = g.concat_signal("postprocessing/dsignal/extraction_general_None_volume")
clean_vols = vols.loc[vols.notna().sum(axis=1) > 0.8 * 180]

import seaborn as sns
import matplotlib.pyplot as plt

agg = g.concat_signal("postprocessing/experiment_wide/aggregated")

# GFP median
gfp = g.concat_signal("extraction/GFP_bgsub/np_max/median")
clean_gfp = gfp.loc[gfp.notna().sum(axis=1) > 0.8 * 180]
sns.heatmap(clean_gfp, robust=True)
plt.show()

# Check max2p5 in case it matters (Pic2 might localise in the mitochondria)
max2p5 = g.concat_signal("extraction/GFP/np_max/max2p5pc")
clean_max2p5 = max2p5.loc[max2p5.notna().sum(axis=1) > 0.8 * 180]
sns.heatmap(clean_max2p5, robust=True)
plt.show()

# Volumes
vol = g.concat_signal("extraction/general/None/volume")
clean_vol = vol.loc[vol.notna().sum(axis=1) > 0.8 * 180]

# Plot max2p5 as a function of time
melt_m5 = clean_max2p5.melt(ignore_index=False, var_name="tp").reset_index()
melt_gfp = clean_gfp.melt(ignore_index=False, var_name="tp").reset_index()
melt_vols = clean_vols.melt(ignore_index=False, var_name="tp").reset_index()
melt_vol = clean_vol.melt(ignore_index=False, var_name="tp").reset_index()

sns.lineplot(data=melt_m5, x="tp", y="value", hue="position")
sns.lineplot(data=melt_gfp, x="tp", y="value", hue="position")
sns.lineplot(data=melt_vols, x="tp", y="value", hue="group")
sns.lineplot(data=melt_vol, x="tp", y="value", hue="group")

# Histogram of aggregated metrics
clean = agg["general_volume_max"].reset_index()
sns.histplot(data=clean, x="general_volume_max", hue="position")
plt.show()
